@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Startsida</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>
                    En hanterare för registerbeskrivningar. <br>
                    Om du får bekymmer, börja med att läsa  <a href="https://gitlab.com/simonblund/reggee/wikis/user-guide/Getting-started">Wikisidan</a>,
                    om lösningen på ditt bekymmer inte finns där kan du ta kontakt med din systemadministratör.
                    <br>
                    Om du hittar fel eller buggar i systemet kan du starta ett ärende i servicedesk för reggee genom att skicka ett mejl till <a href="mailto:incoming+simonblund/reggee@incoming.gitlab.com">servicedesk</a>.
                    Det samma gäller för funktioner du saknar.
                    </p>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Enheter</div>

                <div class="card-body">
                <ul>
                    @foreach($units as $unit)
                    <a href="{{ route('unit_id', ['id'=>$unit->id]) }}">
                    <li>
                            {{ $unit->name }}
                    </li>
                    </a>
                    @endforeach
                </ul>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
