<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>{{ $senderName or '' }}</title>
	<style>
		a:hover {
			text-decoration: underline !important;
		}
		td.promocell p {
			color:#e1d8c1;
			font-size:16px;
			line-height:26px;
			font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;
			margin-top:0;
			margin-bottom:0;
			padding-top:0;
			padding-bottom:14px;
			font-weight:normal;
		}
		td.contentblock h4 {
			color:#444444 !important;
			font-size:16px;
			line-height:24px;
			font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;
			margin-top:0;
			margin-bottom:10px;
			padding-top:0;
			padding-bottom:0;
			font-weight:normal;
		}
		td.contentblock h4 a {
			color:#444444;
			text-decoration:none;
		}
		td.contentblock p {
			color:#888888;
			font-size:13px;
			line-height:19px;
			font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;
			margin-top:0;
			margin-bottom:12px;
			padding-top:0;
			padding-bottom:0;
			font-weight:normal;
		}
		td.contentblock p a {
			color:#3ca7dd;
			text-decoration:none;
		}
		@media only screen and (max-device-width: 480px) {
			div[class="header"] {
				font-size: 16px !important;
			}
			table[class="table"], td[class="cell"] {
				width: 300px !important;
			}
			table[class="promotable"], td[class="promocell"] {
				width: 325px !important;
			}
			td[class="footershow"] {
				width: 300px !important;
			}
			table[class="hide"], img[class="hide"], td[class="hide"] {
				display: none !important;
			}
			img[class="divider"] {
				height: 1px !important;
			}
			td[class="logocell"] {
				padding-top: 15px !important;
				padding-left: 15px !important;
				width: 300px !important;
			}
			img[id="screenshot"] {
				width: 325px !important;
				height: 127px !important;
			}
			img[class="galleryimage"] {
				width: 53px !important;
				height: 53px !important;
			}
			p[class="reminder"] {
				font-size: 11px !important;
			}
			h4[class="secondary"] {
				line-height: 22px !important;
				margin-bottom: 15px !important;
				font-size: 18px !important;
			}
		}
		
		{{ $css or '' }}
	</style>
</head>
<body bgcolor="#e4e4e4" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing: antialiased;width:100%;-webkit-text-size-adjust:none;">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td bgcolor="#ffffff" width="100%">

					<table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="table">
						<tr bgcolor="#ffffff">
							<td width="600" class="cell">

								<table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" class="table">
									<tr>
										<td width="250" bgcolor="#ffffff" class="logocell">
											<br class="hide">
												<img src="{{ $message->embed(public_path() . '/img/logo.png') }}" width="200" height="100px" alt="{{ $senderName or '' }}" style="-ms-interpolation-mode:bicubic;">
											<br>
											
										<td align="right" width="350" class="hide" style="color:#a6a6a6;font-size:12px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;text-shadow: 0 1px 0 #ffffff;" valign="top" bgcolor="#ffffff"><br></td>
									</tr>
								</table>

								<span><br>
								<table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" class="table">
								<tr>
										<td>
										<span><br></span>
										</td>
									</tr>
									
									<tr>
										<td>
										En ny registerbeskrivning har skapats för registret {{ $register->name}} och finns bifogad i detta mejl.
										</td>
									</tr>
									<tr>
										<td>
										<span><br></span>
										</td>
									</tr>
									<tr>
									<td>
										Läs registerbeskrivningen och kontrollera att all information stämmer, skriv sedan ut beskrivningen i två exemplar och byt ut de gamla registerbeskrivningarna.
										</td>
									</tr>

									<tr>
										<td>
										<span><br></span>
										</td>
									</tr>
									<tr>
									<td>
										Du får detta mejl i egenskap av ansvarig för register vid enheten eller dataskyddsombud för enheten som förvaltar registret.
										</td>
									</tr>
									@isset($register->editor)
									<tr>
										<td>
										<span><br></span>
										</td>
									</tr>
									<tr>
									<td>
										Ändringar har gjorts: {{$register->updated_at}} av
										{{$register->editor->name}}
										</td>
									</tr>
									@endisset

									@isset($register->reviewer)
									<tr>
										<td>
										<span><br></span>
										</td>
									</tr>
									<tr>
									<td>
										Granskning har gjorts: {{$register->date_reviewed}} av
										{{$register->reviewer->name}}
										</td>
									</tr>
									@endisset

									<tr>
										<td>
										<span><br></span>
										</td>
									</tr>
									<tr>
									<td>
										I fall av säkerhetsincident {{$register->breach_information_public}}
										</td>
									</tr>
								</table>

								

							</td>
						</tr>
					</table>

</body>
</html>