@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $unit->name}} <span class="float-right"><a href="{{ route('unit_edit', ['id'=>$unit->id])  }}">Redigera</a></span></div>

                <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                        <th scope="row">Namn</th>
                        <td>{{ $unit->name}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Gatuadress</th>
                        <td>{{ $unit->streetaddress}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Stad</th>
                        <td>{{ $unit->zip}}, {{ $unit->city}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Kontaktperson</th>
                        <td>{{ $unit->contact}}</td>
                        </tr>
                        <tr>
                        <th scope="row">E-post</th>
                        <td>{{ $unit->email}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Telefonnummer</th>
                        <td>{{ $unit->telephone}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Dataskyddsombud</th>
                        <td>{{ $unit->user->name}}</td>
                        </tr>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    
        <div class="col-md-10" style="padding-top: 2rem">
            <div class="card">
                <div class="card-header">Register på {{ $unit->name}}</div>

                <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Namn</th>
                            <th>Internt ID</th>
                            <th>Uppdaterat</th>
                            <th>Granskat</th>
                            <th>Redigera</th>
                            <th>Visa RB</th>
                            <th>E-posta RB</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unit->register as $register)
                        <tr>
                        <td><a href="{{ route('reg_id', ['id'=>$register->id]) }}">{{ $register->name}}</a></td>
                        <td>{{ $register->internal_identification}}</td>
                        <td>{{ $register->updated_at}}</td>
                        <td>{{ $register->date_reviewed}}</td>
                        <td><a href="{{ route('reg_edit', ['id'=>$register->id]) }}">Redigera</a></td>
                        <td><a href="{{ route('pdf_view', ['id'=>$register->id]) }}">Visa</a></td>
                        <td><a href="{{ route('pdf_email', ['id'=>$register->id]) }}">E-post</a></td>
                        </tr>
                        @endforeach
                       

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
