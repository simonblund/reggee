@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Skapa enhet</div>

                <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <form action="{{ route('unit_new') }}" method="POST">
                    @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Enhet</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Enhetens namn" aria-label="Enhetens namn" name="name" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Gata</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Gata" aria-label="Enhetens namn" name="streetaddress" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Stad</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Stad" aria-label="Enhetens namn" name="city" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Postnummer</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Postnummer" aria-label="Enhetens namn" name="zip" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Kontaktperson</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Kontaktperson" aria-label="Enhetens namn" name="contact" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">E-post</span>
                            </div>
                            <input type="e-mail" class="form-control" placeholder="E-post" aria-label="Enhetens namn" name="email" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Tel.nr.</span>
                            </div>
                            <input type="tel" class="form-control" placeholder="Telefonnummer" aria-label="Enhetens namn" name="telephone" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Dataskyddsombud</label>
                            </div>
                            <select class="custom-select" id="inputGroupSelect01" name="user_id" required>
                                <option selected>Välj...</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Spara">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
