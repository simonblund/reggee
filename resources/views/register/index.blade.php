@extends('layouts.app')

@section('content')
<div class="container">
@if (session('status'))
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Register</div>

                <div class="card-body">
                    
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    
                    <p>
                    Kökar Kommuns GDPR-hanterare.
                    </p>
                    
                </div>
            </div>
        </div>
    </div>
    @endif
    @foreach($units as $unit)
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ $unit->name }}</div>

                <div class="card-body">
                <table class="table table-striped table-light">
                    <thead>
                        <th>Registrets namn</th>
                        <th>Internt ID</th>
                        <th>Senaste genomgång</th>
                        <th>Genomgång utförd av</th>
                        <th>Redigera</th>
                        <th>Visa RB</th>
                        <th>E-posta RB</th>
                    </thead>
                    <tbody>
                    @foreach($unit->register as $register)
                    <tr>
                    <td><a href="{{ route('reg_id', ['id'=>$register->id]) }}">{{ $register->name }}</a></td>
                    <td>{{ $register->internal_identification }}</td>
                    @isset($register->reviewer)
                    <td>{{ $register->date_reviewed }}</td>
                    <td>{{ $register->reviewer->name }}</td>
                    @endisset
                    @empty($register->reviewer)
                    <td>-</td>
                    <td>-</td>
                    @endempty
                    <td><a href="{{ route('reg_edit', ['id'=>$register->id]) }}">Redigera</a></td>
                    <td><a href="{{ route('pdf_view', ['id'=>$register->id]) }}">Visa</a></td>
                    <td><a href="{{ route('pdf_email', ['id'=>$register->id]) }}">E-post</a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                    
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
