@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-header">{{ $reg->name}} <span class="float-right"><a href="{{ route('reg_edit', ['id'=>$reg->id]) }}">Redigera</a> -  <a href="{{ route('pdf_email', ['id'=>$reg->id]) }}">E-posta RB</a></span></div>

                <div class="card-body">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                        <th scope="row">Namn</th>
                        
                        <td>{{ $reg->name}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Digitalt system registret finns i (program, dator, eller tjänst)</th>
                        
                        <td>{{ $reg->digital}}</td>
                        </tr>

                        <tr>
                        <th scope="row">Personuppgiftsbiträde</th>
                        
                        <td>{{ $reg->processor_name}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Personuppgiftsbiträdets organisationsnummer</th>
                        
                        <td>{{ $reg->processor_orgnr}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Personuppgiftsbiträdets adress</th>
                        
                        <td>{{ $reg->processor_address}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Personuppgiftsbiträdets stad och postnummer</th>
                        
                        <td>{{ $reg->processor_city_zip}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Personuppgiftsbiträdets telefonnummer</th>
                        
                        <td>{{ $reg->processor_phone}}</td>
                        </tr>
                        
                        <tr>
                        <th scope="row">Ändamålet med behandlingen eller registret</th>
                        <td>{{ $reg->reason }}</td>
                        </tr>
                        <tr>
                        <th scope="row">Innehåller information om följande personkategorier</th>
                        <td>{{ $reg->people_in_it}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Kan registret innehålla information om personer under 13-års ålder?</th>
                        <td>@if($reg->under_13) Ja @else Nej @endif</td>
                        </tr>
                        <tr>
                        <th scope="row">Information i registret</th>
                        <td>{{ $reg->information_points}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Förekommer särskilt skyddsvärd information i registret? Om ja, vilken.</th>
                        <td>{{ $reg->sensitive_data}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Följande personkategorier får tillgång till registret och om bara vissa delar markera med parentes.</th>
                        <td>{{ $reg->people_with_access}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Hanteras informationen utanför EU/EES. Om ja var och varför samt vilken information</th>
                        <td>{{ $reg->outside_eu}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Tidsfrister för arkivering eller radering av datainnehåll.</th>
                        <td>{{ $reg->save_or_archive_time}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Juridisk grund för lagring av information. Lag eller avtal.</th>
                        <td>{{ $reg->legal_reason}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Krävs samtycke?</th>
                        <td>@if($reg->consent_boolean) Ja @else Nej @endif </td>
                        </tr>
                        <tr>
                        <th scope="row">Information om samtycke. När inhämtas det?</th>
                        <td>{{ $reg->consent_information}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Hur samlas data till registret in?</th>
                        <td>{{ $reg->collection_information}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Beskriv tekniska och organisatoriska säkerhetsåtgärder som vidtagits för att minska risken för informationsläckage</th>
                        <td>{{ $reg->safety_systems}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Om registret finns i pappersform, hur många exemplar och var finns de?</th>
                        <td>{{ $reg->on_paper}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Om registret finns i digital form, hur många exemplar och var finns de?</th>
                        <td>{{ $reg->on_computer}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Enhet</th>
                        <td>{{ $reg->unit->name}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Internt ID</th>
                        <td>{{ $reg->internal_identification}}</td>
                        </tr>
                        <tr>
                        <th scope="row">Kommentarer / Anteckningar som inte syns i registerbeskrivningen</th>
                        <td>{{ $reg->comments}}</td>
                        </tr>


                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8" style="padding-top:2em">
            <div class="card">
                <div class="card-header">Säkerhetsinformation</div>

                <div class="card-body">
                    <span>Instruktioner i fall av dataläcka eller risktillbud INTERNA</span>
                    <p>{{ $reg->breach_information_internal}}</p>
                    <hr>
                    <span>Instruktioner i fall av dataläcka eller risktillbud PUBLIKA</span>
                    <p>{{ $reg->breach_information_public}}</p>
                </div>
            </div>
        </div>

        <div class="col-md-8" style="padding-top:2em">
            <div class="card">
                <div class="card-header">Granskningsinformation</div>

                <div class="card-body">
                    <span class="font-weight-bold">Registret registrerat</span>
                    <p>{{ $reg->created_at}}</p>
                    <hr>
                    <span class="font-weight-bold">Registret uppdaterat</span>
                    @isset($reg->editor)
                    <p>{{ $reg->updated_at}} / {{ $reg->editor->name}}</p>
                    @endisset
                    <hr>
                    <span class="font-weight-bold">Granskat</span>
                    <p>
                    @if($reg->date_reviewed)
                    {{ $reg->date_reviewed}} / {{ $reg->reviewer->name }}
                    @else
                    aldrig Granskat
                    @endif
                    </p>
                    <br>

                    <span class="font-weight-bold">Granskning</span>
                    <p>Vid granskning tar du kontakt med registrets innehavare och kontrollerar att registret existerar samt att registrets utrensning och uppdatering utförts. Då du fått godtagbart svar kan du markera registret som granskat.</p>
                    <hr>

                    <form action="{{ route('reg_review', ['id'=>$reg->id]) }}" method="POST">
                        @csrf
                        <input type="checkbox" name="reviewed" id="reviewed" value="1"><label for="reviewed">Registret granskat</label>
                        <input type="hidden" name="id" value="{{$reg->id}}">
                        <input type="submit" class="btn btn-success" value="Granska">
                    </form>
                </div>
            </div>
        </div>
    
    </div>
    
</div>
@endsection
