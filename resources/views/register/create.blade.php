@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Skapa register</div>

                <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <form action="{{ route('reg_new') }}" method="POST">
                    @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Registret eller behandlingens namn</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Registrets namn" aria-label="Registrets namn" name="name" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="digital">Digitalt system registret finns i (program, dator, eller tjänst)</label>
                            <input type="text" class="form-control" id="digital" name="digital" placeholder="Digitalt system">
                        </div>

                        <hr>
                        <br>

                        <div class="form-group bg-warning">
                            <label for="processor_name">Personuppgiftsbiträde organisation (och kontaktperson) [om det finns, annars lämnas alla gula fält tomma]</label>
                            <input type="text" class="form-control" id="processor_name" name="processor_name" placeholder="Personuppgiftsbiträde organisation och kontaktperson">
                        </div>

                        <div class="form-group bg-warning">
                            <label for="processor_orgnr">Personuppgiftsbiträdets organisationsummer</label>
                            <input type="text" class="form-control" id="processor_orgnr" name="processor_orgnr" placeholder="12345-12">
                        </div>
                        <div class="form-group bg-warning">
                            <label for="processor_address">Personuppgiftsbiträdets gatuadress</label>
                            <input type="text" class="form-control" id="processor_address" name="processor_address" placeholder="Personuppgiftsvägen 12A 3">
                        </div>
                        <div class="form-group bg-warning">
                            <label for="processor_city_zip">Personuppgiftsbiträdets stad och postnummer</label>
                            <input type="text" class="form-control" id="processor_city_zip" name="processor_city_zip" placeholder="12345 STAD">
                        </div>
                        <div class="form-group bg-warning">
                            <label for="processor_phone">Personuppgiftsbiträdets telefonnummer</label>
                            <input type="text" class="form-control" id="processor_phone" name="processor_phone" placeholder="12345">
                        </div>
                        <hr>
                        <br>

                        <div class="form-group">
                            <label for="reason">Ändamålet med behandlingen eller registret</label>
                            <textarea type="textarea" class="form-control" id="reason" name="reason" placeholder="Ändamål">
                            </textarea>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="people_in_it">Innehåller information om följande personkategorier</label>
                            <textarea type="textarea" class="form-control" id="people_in_it" name="people_in_it" placeholder="Personkategorier,">
                            </textarea>
                            <small id="people_in_it_help" class="form-text text-muted">Separera med kommatecken, börja med stor bokstav.</small>
                        </div>

                        <div class="form-group">
                            <label for="under_13">Kan registret innehålla information om personer under 13-års ålder?</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="under_13" id="exampleRadios1" value="1" checked>
                                <label class="form-check-label" for="under_13">
                                    Ja
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="under_13" id="under_13" value="0">
                                <label class="form-check-label" for="under_13">
                                    Nej
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="information_points">Information i registret</label>
                            <textarea type="textarea" class="form-control" id="information_points" name="information_points" placeholder="T.ex. Personnamn, telefonnummer,">
                            </textarea>
                            <small id="information_points_help" class="form-text text-muted">Separera med kommatecken, börja med stor bokstav.</small>
                        </div>

                        <div class="form-group">
                            <label for="sensitive_data">Förekommer särskilt skyddsvärd information i registret? Om ja, vilken.</label>
                            <textarea type="textarea" class="form-control" id="sensitive_data" name="sensitive_data" placeholder="T.ex. Sjukdomar, Religionstillhörighet,">
                            </textarea>
                            <small id="sensitive_data_help" class="form-text text-muted">Separera med kommatecken, börja med stor bokstav.</small>
                        </div>

                        <div class="form-group">
                            <label for="people_with_access">Följande personkategorier får tillgång till registret och om bara vissa delar markera med parentes.</label>
                            <textarea type="textarea" class="form-control" id="people_with_access" name="people_with_access" placeholder="T.ex. Personal på Sommarängen, Anhöriga(bara kontaktuppgifter),">
                            </textarea>
                            <small id="people_with_access_help" class="form-text text-muted">Separera med kommatecken, börja med stor bokstav.</small>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="outside_eu">Hanteras informationen utanför EU/EES. Om ja var och varför samt vilken information</label>
                            <textarea type="textarea" class="form-control" id="outside_eu" name="outside_eu" placeholder="Utanför EU">
                            </textarea>
                            <small id="outside_eu_help" class="form-text text-muted">Separera med kommatecken, börja med stor bokstav.</small>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="save_or_archive_time">Tidsfrister för arkivering eller radering av datainnehåll.</label>
                            <textarea type="textarea" class="form-control" id="save_or_archive_time" name="save_or_archive_time" placeholder="T.ex. Arkivering 6 år, radering 70 år efter personens död.">
                            </textarea>
                            <small id="outside_eu_help" class="form-text text-muted">Separera med kommatecken, börja med stor bokstav.</small>
                        </div>

                        <div class="form-group">
                            <label for="legal_reason">Juridisk grund för lagring av information. Lag eller avtal.</label>
                            <textarea type="textarea" class="form-control" id="legal_reason" name="legal_reason" placeholder="T.ex. Bokförinslagen, ÅLFs(2011:12) § 8">
                            </textarea>
                            <small id="legal_reason_help" class="form-text text-muted">Separera med kommatecken, börja med stor bokstav.</small>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="consent_boolean">Krävs samtycke?</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="consent_boolean" id="consent_boolean" value="1" >
                                <label class="form-check-label" for="consent_boolean">
                                    Ja
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="consent_boolean" id="consent_boolean" value="0" checked>
                                <label class="form-check-label" for="consent_boolean">
                                    Nej
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="consent_information">Information om samtycke. När inhämtas det?</label>
                            <textarea type="textarea" class="form-control" id="consent_information" name="consent_information" placeholder="T.ex. Samtycke inhämtas i samband med inskrivning">
                            </textarea>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="collection_information">Hur samlas data till registret in?</label>
                            <textarea type="textarea" class="form-control" id="collection_information" name="collection_information" placeholder="T.ex. via blankett som registrerade fyller i.">
                            </textarea>
                            <small id="collection_information_help" class="form-text text-muted">Flytande text.</small>
                        </div>

                        <div class="form-group">
                            <label for="safety_systems">Beskriv tekniska och organisatoriska säkerhetsåtgärder som vidtagits för att minska risken för informationsläckage.</label>
                            <textarea type="textarea" class="form-control" id="safety_systems" name="safety_systems" placeholder="T.ex. Låst skåp som bara behörig personal har tillgång till.">
                            </textarea>
                            <small id="safety_systems_help" class="form-text text-muted">Flytande text.</small>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="on_paper">Om registret finns i pappersform, hur många exemplar och var finns de?</label>
                            <textarea type="textarea" class="form-control" id="on_paper" name="on_paper" placeholder="T.ex. Arkivskåpet i kansliet(1st), Tarifolder i kansliet(1st).">
                            </textarea>
                            <small id="on_paper_help" class="form-text text-muted">Kommaseparerat, Stor bokstav, antal inom parentes.</small>
                        </div>

                        <div class="form-group">
                            <label for="on_computer">Om registret finns i digital form, hur många exemplar och var finns de?</label>
                            <textarea type="textarea" class="form-control" id="on_computer" name="on_computer" placeholder="T.ex. Dator i kansliet(1st), Dator i kansliet(1st).">
                            </textarea>
                            <small id="on_computer_help" class="form-text text-muted">Kommaseparerat, Stor bokstav, antal inom parentes.</small>
                        </div>

                        <hr>
                        <hr>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="unit">Enhet</label>
                            </div>
                            <select class="custom-select" id="unit" name="unit_id" required>
                                <option selected>Välj...</option>
                                @foreach($units as $unit)
                                    <option value="{{$unit->id}}">{{$unit->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <hr>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="internal_identification">Internt ID</span>
                            </div>
                            <input type="text" class="form-control" placeholder="T.ex. SKO-1A" aria-label="Internt-ID" name="internal_identification" required>
                        </div>

                        <div class="form-group">
                            <label for="comments">Kommentarer / Anteckningar som inte syns i registerbeskrivningen</label>
                            <textarea type="textarea" class="form-control" id="comments" name="comments" placeholder="T.ex. Kontrollera med Simon vad jag ska göra i det här fallet.">
                            </textarea>
                            <small id="comments_help" class="form-text text-muted">Flytande text, dateras automatiskt.</small>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="breach_information_internal">Instruktioner i fall av dataläcka eller risktillbud INTERNA</label>
                            <textarea type="textarea" class="form-control" id="breach_information_internal" name="breach_information_internal" placeholder="T.ex. 1) TA KONTAKT MED DRABBADE">
                            </textarea>
                            <small id="breach_information_internal_help" class="form-text text-muted">Flytande text</small>
                        </div>

                        <div class="form-group">
                            <label for="breach_information_public">Instruktioner i fall av dataläcka eller risktillbud OFFENTLIGT</label>
                            <textarea type="textarea" class="form-control" id="breach_information_public" name="breach_information_public" placeholder="T.ex. 1) TA KONTAKT MED DRABBADE">
                            </textarea>
                            <small id="breach_information_public_help" class="form-text text-muted">Flytande text</small>
                        </div>

                        <input class="btn btn-primary" type="submit" value="Spara">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
