@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Redigera användare') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{route('user_id', ['id' => $user->id ])}}" aria-label="{{ __('Register user') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Namn') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-postadress') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="is_admin" class="col-md-4 col-form-label text-md-right">Administratör</label>

                            <div class="col-md-6">
                                <div class="form-group form-check">
                                    <input type="radio" class="form-check-input" id="is_admin" name="is_admin" value="1" @if($user->is_admin) checked @endif>
                                    <label class="form-check-label" for="is_admin">Ja</label>
                                    <br>
                                    <input type="radio" class="form-check-input" id="is_admin" name="is_admin" value="0" @if($user->is_admin == 0) checked @endif>
                                    <label class="form-check-label" for="is_admin">Nej</label>
                                </div>
                                @if ($errors->has('is_admin'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('is_admin') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="unit" class="col-md-4 col-form-label text-md-right">Kan skapa enheter</label>

                            <div class="col-md-6">
                                <div class="form-group form-check">
                                    <input type="radio" class="form-check-input" id="can_unit" name="can_unit" value="1" @if($user->can_unit) checked @endif>
                                    <label class="form-check-label" for="can_unit">Ja</label>
                                    <br>
                                    <input type="radio" class="form-check-input" id="can_unit" name="can_unit" value="0"  @if($user->can_unit==0) checked @endif>
                                    <label class="form-check-label" for="can_unit">Nej</label>
                                </div>
                                @if ($errors->has('can_unit'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('can_unit') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="can_register" class="col-md-4 col-form-label text-md-right">Kan skapa register</label>

                            <div class="col-md-6">
                                <div class="form-group form-check">
                                    <input type="radio" class="form-check-input" id="can_register" name="can_register" value="1" @if($user->can_register) checked @endif>
                                    <label class="form-check-label" for="can_unit">Ja</label>
                                    <br>
                                    <input type="radio" class="form-check-input" id="can_register" name="can_register" value="0" @if($user->can_register==0) checked @endif >
                                    <label class="form-check-label" for="can_register">Nej</label>
                                </div>
                                @if ($errors->has('can_register'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('can_register') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="connected_unit">Enhet</label>
                            </div>
                            <select class="custom-select" id="connected_unit" name="connected_unit" required>
                                <option selected value="0">Välj...</option>
                                @foreach($units as $unit)
                                    <option value="{{$unit->id}}">{{$unit->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <p class="text-center">
                                Lösenordsändringar görs genom att användaren klickar att de glömt lösenordet.</p>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Spara ändringar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
