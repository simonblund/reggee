@extends('layouts.app')

@section('content')
<div class="container">
@if (session('status'))
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Användare</div>

                <div class="card-body">
                    
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Användare <span class="small"><a href="{{route('user_new')}}">Skapa ny användare</a></span></div>

                <div class="card-body">
                <table class="table table-striped table-light">
                    <thead>
                        <th>Namn</th>
                        <th>E-post</th>
                        <th>Admin</th>
                        <th>Kan register</th>
                        <th>Kan enhet</th>
                        <th>Sammankopplad enhet</th>
                        <th>Redigera</th>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->is_admin }}</td>
                    <td>{{ $user->can_register }}</td>
                    <td>{{ $user->can_unit }}</td>
                    <td>{{ $user->connected_unit }}</td>
                    <td><a href="{{route('user_id', ['id' => $user->id])}}">Redigera</a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
