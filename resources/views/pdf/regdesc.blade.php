<!DOCTYPE html>
<html>
    <head>
        <style>
        @page {
                header: page-header;
                footer: page-footer;
            }
        body {
	            font-family: 'DejaVuSans', sans-serif;
            }
        table {
            border-collapse: collapse;
                border: 1px solid black;
                width: 100%;
            }
        td {
            border-collapse: collapse;
                border: 1px solid black;
                height: 45px;

        }
        table,td.header{
            border-collapse: collapse;
                border: 0px
        }
        .intro{
            height: 2em;
            background-color: #003F91;
            vertical-align: middle;
            margin-top: 0.5em;
            padding-top: 0.5em;
        }
        h1.intro{
            text-align: center;
            vertical-align: middle;
            color: white;
        }
        hr {
            display: none;
            color: #FFF;
            height: 5px;
        }

        .personuppgiftsansvarig{
            //background-color: #E5F4E3;
            padding-left: 15px;
            
        }
        .DSO{
            //background-color: #E5F4E3;
            padding-left: 15px;
        }
        .behandling{
            //background-color: #E5F4E3;
            padding-left: 15px;
        }
        .mottagare{
            //background-color: #E5F4E3;
            padding-left: 15px;
        }

        .sakerhet{
            //background-color: #E5F4E3;
            padding-left: 15px;
        }

        table.personuppgiftsansvarig{
            width: 100%;
            
        }
        .pua-desc{
            width: 300px;
            font-size: 16px;
            padding-left: 15px;
        }
        .pua-value{
            font-size: 16px;
        }
        </style>
    </head>
    <body>
            <htmlpageheader name="page-header">
            
            <table width="100%" class="header">
                <tr>
                    <td class="header" width="30%">{{ $enva['name'] }}</td>
                    <td class="header" width="60%" style="text-align: center;">REGISTERBESKRIVNING - {{ $reg->name }}</td>
                    <td class="header" width="10%" align="right">{PAGENO}/{nbpg}</td>
                </tr>
            </table>
            
            </htmlpageheader>
            <htmlpagefooter name="page-footer">
            
            <table width="100%" class="footer">
                <tr>
                    <td class="header" width="30%">{{ $reg->internal_identification }}</td>
                    <td class="header" width="40%" style="text-align: center;">{{ $reg->name }}</td>
                    <td class="header" width="30%" align="right">{{ $reg->updated_at }}</td>
                </tr>
            </table>
            
            </htmlpagefooter>
            <br>
            <div class="intro">
                
                <h1 class="intro">REGISTERBESKRIVNING</h1>
            </div>
            <hr class="intro">
            
            <div class="personuppgiftsansvarig">
                <h3>PERSONUPPGIFTSANSVARIG</h3>
                <table class="personuppgiftsansvarig">
                    <tr>
                        <td class="pua-desc">
                        Namn:
                        </td>
                        <td class="pua-value">
                        {{ $unit->name }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        Gatuadress:
                        </td>
                        <td class="pua-value">
                        {{ $unit->streetaddress }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        Stad och postnummer:
                        </td>
                        <td class="pua-value">
                        {{ $unit->zip }} {{ $unit->city }}
                        </td>
                    </tr>
                    <tr><th><br></th></tr>
                    <tr>
                        <td class="pua-desc">
                        Kontaktperson:
                        </td>
                        <td class="pua-value">
                        {{ $unit->contact }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        E-post och Telefonnummer:
                        </td>
                        <td class="pua-value">
                        {{ $unit->email }}, {{ $unit->telephone }}
                        </td>
                    </tr>
                    
                </table>
            </div>
            <hr>
            <div class="DSO">
                <h3>DATSKYDDSOMBUD</h3>
                <table class="DSO">
                    <tr>
                        <td class="pua-desc">
                        Namn:
                        </td>
                        <td class="pua-value">
                        {{ $DSO->name }}
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="pua-desc">
                        E-post:
                        </td>
                        <td class="pua-value">
                        {{ $DSO->email }}
                        </td>
                    </tr>
                    

                </table>
            </div>
            <hr>
            <div class="DSO">
                <h3>PERSONUPPGIFTSBITRÄDE (om applicerbart)</h3>
                <table class="DSO">
                    <tr>
                        <td class="pua-desc">
                        Namn och kontaktperson:
                        </td>
                        <td class="pua-value">
                        {{ $reg->processor_name }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        Organisationsnummer:
                        </td>
                        <td class="pua-value">
                        {{ $reg->processor_orgnr }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        Adress:
                        </td>
                        <td class="pua-value">
                        {{ $reg->processor_address }}
                        <br>
                        {{ $reg->processor_city_zip }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        Telefonnummer:
                        </td>
                        <td class="pua-value">
                        {{ $reg->processor_phone }}
                        </td>
                    </tr>
                    

                </table>
            </div>
            <pagebreak>
            <br>
            <div class="behandling">
                <h3>BEHANDLINGEN AV PERSONUPPGIFTER</h3>
                <table class="behandling">
                    <tr>
                        <td class="pua-desc">
                        Behandlingens namn:
                        </td>
                        <td class="pua-value">
                        {{ $reg->name }}
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="pua-desc">
                        Internt ID:
                        </td>
                        <td class="pua-value">
                        {{ $reg->internal_identification }}
                        </td>
                    </tr>
                    <tr><th><br></th></tr>
                    <tr>
                        <td class="pua-desc">
                        Ändamålet med behandlingen:
                        </td>
                        <td class="pua-value">
                        {{ $reg->reason }}
                        </td>
                    </tr>
                    <tr><th><br></th></tr>
                    <tr>
                        <td class="pua-desc">
                        Beskrivning av de kategorier av personer som behandlingen berör:
                        </td>
                        <td class="pua-value">
                        {{ $reg->people_in_it }}
                        </td>
                    </tr>
                    <tr><th><br></th></tr>
                    <tr>
                        <td class="pua-desc">
                        Beskrivning av de uppgifter behandlingen innehåller:
                        </td>
                        <td class="pua-value">
                        {{ $reg->information_points }}
                        </td>
                    </tr>
                    <tr><th><br></th></tr>
                    <tr>
                        <td class="pua-desc">
                        Juridisk eller avtalsmässig grund för behandlingen:
                        </td>
                        <td class="pua-value">
                        {{ $reg->legal_reason }}
                        <br>
                        {{ $reg->consent_information }}
                        @if($reg->consent_boolean)
                        Behandlingen grundar sig på samtycke.
                        @endif
                        </td>
                    </tr>

                </table>
            </div>
            <hr>

            <div class="behandling">
                <h3>MOTTAGARE</h3>
                <table class="mottagare">
                    <tr>
                        <td class="pua-desc">
                        Kategorier av mottagare till vilka personuppgifterna lämnas:
                        </td>
                        <td class="pua-value">
                        {{ $reg->people_with_access }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        Om registret hanteras utanför EU/EES, av vem och varför:
                        </td>
                        <td class="pua-value">
                        {{ $reg->outside_eu }}
                        </td>
                    </tr>
                    <tr><th><br></th></tr>
                    <tr>
                        <td class="pua-desc">
                        Bevarande, arkivering och gallring:
                        </td>
                        <td class="pua-value">
                        {{ $reg->save_or_archive_time }}
                        </td>
                    </tr>
                    
                </table>
            </div>
            <pagebreak>
            <br>

            <div class="sakerhet">
                <h3>SÄKERHET</h3>
                <table class="sakerhet">
                    <tr>
                        <td class="pua-desc">
                        Säkerhetsåtgärder:
                        </td>
                        <td class="pua-value">
                        {{ $reg->safety_systems }}
                        </td>
                    </tr>
                </table>
            </div>
            <hr>
            <div class="sakerhet">
                <h3>TEKNISK INFORMATION</h3>
                <table class="teknisk">
                    <tr>
                        <td class="pua-desc">
                        Senaste genomgång:
                        </td>
                        <td class="pua-value">
                        {{ $reg->date_reviewed }}
                        </td>
                    </tr>
                    <tr>
                        <td class="pua-desc">
                        Uppdaterad:
                        </td>
                        <td class="pua-value">
                        {{ $reg->updated_at }}
                        </td>
                    </tr>
                    
                </table>
            </div>
            <div>
            <p>
            Skapat med registerhanteraren Reggee.
            </p></div>
            

    </body>
</html>
