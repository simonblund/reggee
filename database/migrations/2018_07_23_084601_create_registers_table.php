<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('digital')->nullable();
            $table->text('reason');
            $table->text('people_in_it');
            $table->boolean('under_13');
            $table->text('information_points');
            $table->text('sensitive_data')->nullable();
            $table->text('people_with_access');
            $table->text('outside_eu')->nullable();
            $table->text('save_or_archive_time');
            $table->text('legal_reason')->nullable();
            $table->boolean('consent_boolean');
            $table->text('consent_information')->nullable();
            $table->text('collection_information');
            $table->text('safety_systems');
            $table->text('on_paper')->nullable();
            $table->text('on_computer')->nullable();
            $table->integer('unit_id');
            $table->string('internal_identification');
            $table->timestamp('date_reviewed')->nullable();
            $table->integer('reviewed_by')->nullable();
            $table->text('comments')->nullable();
            $table->integer('edited_by')->nullable();
            $table->text('hash')->nullable();
            $table->text('breach_information_internal')->nullable();
            $table->text('breach_information_public')->nullable();

            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
