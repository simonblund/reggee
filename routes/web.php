<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login', 301);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');;

//User VIEWS
Route::get('/user/new', 'UserController@create')->name('user_new')->middleware('admin');;
Route::post('/user/new', 'UserController@store')->name('user_new')->middleware('admin');;
Route::get('/user/list', 'UserController@index')->name('user_list')->middleware('admin');;
Route::get('/user/{id}', 'UserController@edit')->name('user_id')->middleware('admin');;
Route::post('/user/{id}', 'UserController@update')->name('user_id')->middleware('admin');;

// UNIT VIEWS
Route::get('/unit/new', 'UnitController@create')->name('unit_new')->middleware('auth');;
Route::get('/unit/{id}', 'UnitController@show')->name('unit_id')->middleware('auth');;
Route::post('/unit/new', 'UnitController@store')->name('unit_new')->middleware('auth');;
Route::get('/unit/{id}/edit', 'UnitController@edit')->name('unit_edit')->middleware('auth');;
Route::patch('/unit/{id}/edit', 'UnitController@update')->name('unit_edit')->middleware('auth');;

// REGISTER VIEWS
Route::get('/register/new', 'RegController@create')->name('reg_new')->middleware('auth');;
Route::get('/register/all', 'RegController@index')->name('reg_list')->middleware('auth');;
Route::get('/register/{id}', 'RegController@show')->name('reg_id')->middleware('auth');;
Route::post('/register/new', 'RegController@store')->name('reg_new')->middleware('auth');;
Route::get('/register/{id}/edit', 'RegController@edit')->name('reg_edit')->middleware('auth');;
Route::patch('/register/{id}/edit', 'RegController@update')->name('reg_edit')->middleware('auth');;
Route::post('/register/{id}/review', 'RegController@review')->name('reg_review')->middleware('auth');;

// PDF GENERATING ROUTES
Route::get('/pdf/register/view/{id}', 'regdescPDFController@generate_regdesc')->name('pdf_view')->middleware('auth');
Route::get('/pdf/register/mail/{id}', 'regdescPDFController@mailregdesc')->name('pdf_email')->middleware('auth');

