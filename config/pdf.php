<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => 'Kökar Kommun',
	'subject'               => '',
	'keywords'              => 'GDPR, Register',
	'creator'               => 'Reggee',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('/temp/')
];
