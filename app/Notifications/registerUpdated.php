<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class registerUpdated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $foundreg;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($foundreg)
    {
        $this->foundreg = $foundreg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/register/'.$this->foundreg->id);
        return (new MailMessage)
                    ->subject('Registerbeskrivning uppdaterad - Reggee')
                    ->greeting('Hej!')
                    ->line('En registerbeskrivning i systemet för registerhantering har uppdaterats.')
                    ->line('Registret som uppdaterats är '.$this->foundreg->name)
                    ->line('Kontrollera ändringarna genom att logga in.')
                    ->action('Ta en titt', url($url))
                    ->line('Du får detta meddelande för att du är DSO eller enhetsansvarig för enheten vars register uppdaterats."');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
