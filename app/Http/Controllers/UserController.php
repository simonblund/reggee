<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Unit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserCreated;
use Illuminate\Support\Facades\Notification;

class UserController extends Controller
{
    use Notifiable;

    public function index()
    {
        $users = User::all();
        return view('auth.list')->with('users', $users);
    }

    public function create()
    {
        $units = Unit::all();
        return view('auth.register')->with('units', $units);
    }

    protected function store(Request $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_admin' => $data['is_admin'],
            'can_unit' => $data['can_unit'],
            'can_register' => $data['can_register'],
            'connected_unit' => $data['connected_unit'],
        ]);

        Notification::send($user, new UserCreated($user));
        return redirect('home');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $units = Unit::all();
        return view('auth.edit')->with('user', $user)->with('units', $units);
    }

    protected function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        
        $founduser = User::findOrFail($request->id);
        $founduser->name = $request['name'];
        $founduser->email = $request['email'];
        if(Auth::user()->is_admin) {
            $founduser->is_admin = $request['is_admin'];
            $founduser->can_unit = $request['can_unit'];
            $founduser->can_register = $request['can_register'];
            $founduser->connected_unit = $request['connected_unit'];
        }

        $founduser->save();
        
        return redirect('home')->with('status', 'Enhet uppdaterad');
        

    }
}
