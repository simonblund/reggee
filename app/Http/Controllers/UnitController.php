<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\User;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $users = User::all();
        return view('unit.create')->with('users', $users);
    }

    protected function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'streetaddress' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'contact' => 'required',
            'user_id' => 'required',
        ]);

        Unit::create([
            'name' => $request['name'],
            'streetaddress' => $request['streetaddress'],
            'city' => $request['city'],
            'zip' => $request['zip'],
            'telephone' => $request['telephone'],
            'email' => $request['email'],
            'contact' => $request['contact'],
            'user_id' => $request['user_id'],
        ]);
        return redirect('home')->with('status', 'Enhet skapad');

    }
    
    protected function show($id)
    {
        $unit = Unit::find($id);
        return view('unit.single')->with('unit', $unit);
    }

    public function edit($id)
    {
        $users = User::all();
        $unit = Unit::find($id);
        return view('unit.edit')->with('users', $users)->with('unit', $unit);
    }

    protected function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'streetaddress' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'contact' => 'required',
            'user_id' => 'required',
        ]);
        
        $foundunit = Unit::findOrFail($request->id);
        $foundunit->name = $request['name'];
        $foundunit->streetaddress = $request['streetaddress'];
        $foundunit->city = $request['city'];
        $foundunit->zip = $request['zip'];
        $foundunit->telephone = $request['telephone'];
        $foundunit->email = $request['email'];
        $foundunit->contact = $request['contact'];
        $foundunit->user_id = $request['user_id'];

        $foundunit->save();
        
        return redirect('home')->with('status', 'Enhet uppdaterad');
        

    }

    protected function index()
    {
        return Unit::all();
    }
}
