<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\User;
use App\Register;
use Mpociot\Versionable\VersionableTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use App\Notifications\registerUpdated;
use Illuminate\Support\Facades\Notification;

class RegController extends Controller
{
    use Notifiable;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $units = Unit::all();
        return view('register.create')->with('units', $units);
    }

    protected function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'reason' => 'required',
            'people_in_it' => 'required',
            'under_13' => 'required',
            'information_points' => 'required',
            'people_with_access' => 'required',
            'save_or_archive_time' => 'required',
            'consent_boolean' => 'required',
            'collection_information' => 'required',
            'safety_systems' => 'required',
            'unit_id' => 'required',
            'internal_identification' => 'required',
        ]);
        
        Register::create($request->all());
        return redirect('home')->with('status', 'Register skapad');

    }
    
    protected function show($id)
    {
        $reg = Register::find($id);
        return view('register.single')->with('reg', $reg);
    }

    protected function review(Request $request)
    {
        $user_id = Auth::id();
        $request->validate([
            'reviewed' => 'required',
            'id' => 'required',
        ]);
        $foundreg = Register::findOrFail($request->id);
        $foundreg->date_reviewed = now();
        $foundreg->reviewed_by = $user_id;
        $foundreg->timestamps = false;
        $foundreg->save();
        return redirect('home')->with('status', 'Register granskat');


    }

    public function edit($id)
    {
        $units = Unit::all();
        $users = User::all();
        $register = Register::find($id);
        return view('register.edit')->with('users', $users)->with('register', $register)->with('units', $units);
    }

    protected function update(Request $request)
    {
        $user_id = Auth::id();
        $request->validate([
            'name' => 'required',
            'reason' => 'required',
            'people_in_it' => 'required',
            'under_13' => 'required',
            'information_points' => 'required',
            'people_with_access' => 'required',
            'save_or_archive_time' => 'required',
            'consent_boolean' => 'required',
            'collection_information' => 'required',
            'safety_systems' => 'required',
            'unit_id' => 'required',
            'internal_identification' => 'required',
        ]);
        
        $foundreg = Register::findOrFail($request->id);
        $foundreg->name = $request['name'];
        $foundreg->digital = $request['digital'];
        $foundreg->reason = $request['reason'];
        $foundreg->people_in_it = $request['people_in_it'];
        $foundreg->under_13 = $request['under_13'];
        $foundreg->information_points = $request['information_points'];
        $foundreg->sensitive_data = $request['sensitive_data'];
        $foundreg->people_with_access = $request['people_with_access'];
        $foundreg->outside_eu = $request['outside_eu'];
        $foundreg->save_or_archive_time = $request['save_or_archive_time'];
        $foundreg->legal_reason = $request['legal_reason'];
        $foundreg->consent_boolean = $request['consent_boolean'];
        $foundreg->consent_information = $request['consent_information'];
        $foundreg->collection_information = $request['collection_information'];
        $foundreg->safety_systems = $request['safety_systems'];
        $foundreg->on_paper = $request['on_paper'];
        $foundreg->on_computer = $request['on_computer'];
        $foundreg->unit_id = $request['unit_id'];
        $foundreg->internal_identification = $request['internal_identification'];
        $foundreg->comments = $request['comments'];
        $foundreg->edited_by = $user_id;
        $foundreg->breach_information_internal = $request['breach_information_internal'];
        $foundreg->breach_information_public = $request['breach_information_public'];
        $foundreg->processor_name = $request['processor_name'];
        $foundreg->processor_orgnr = $request['processor_orgnr'];
        $foundreg->processor_address = $request['processor_address'];
        $foundreg->processor_city_zip = $request['processor_city_zip'];
        $foundreg->processor_phone = $request['processor_phone'];


        $foundreg->save();
        $unit = $foundreg->unit;
        
        //dd($foundreg);
        //$unit->notify(new registerUpdated($unit));
        $DSO = $foundreg->unit->user;

        // Notifications sent when register updated.
        Notification::send($unit, new registerUpdated($foundreg));
        Notification::send($DSO, new registerUpdated($foundreg));
        
        
        return redirect('home')->with('status', 'Registret uppdaterat');
        

    }

    protected function index()
    {
        $units = Unit::all();

        return view('register.index')->with('units', $units);
    }
}
