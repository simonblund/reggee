<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Register;
use App\Unit;
use App\User;
use App;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewRDnotification;
use Auth;
Use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class regdescPDFController extends Controller
{
    function generate_regdesc($id) {
        $unit = Register::find($id)->unit;
        $reg = Register::find($id);
        $DSO = Unit::find($unit->id)->user;


        $enva = [
            'name'=>env('ORG_NAME'),
            'street'=>env('ORG_STREET'),
            'zip'=>env('ORG_ZIP'),
            'city'=>env('ORG_CITY'),
            'country'=>env('ORG_COUNTRY'),
            'email'=>env('ORG_EMAIL'),
        ];
        $data = ['reg'=>$reg, 'unit'=>$unit, 'DSO'=>$DSO, 'enva'=>$enva,];
        
        $pdf = PDF::loadView('pdf.regdesc', $data, [], [
            'format'=> 'A4',
            'author'=> $enva['name'],
            'creator'=> 'Reggee, Registerhanteraren',
        ]);
        return $pdf->stream('document.pdf');
    }
    
    function mailregdesc($id) {
        $unit = Register::find($id)->unit;
        $reg = Register::findOrFail($id);
        $DSO = Unit::find($unit->id)->user;


        $enva = [
            'name'=>env('ORG_NAME'),
            'street'=>env('ORG_STREET'),
            'zip'=>env('ORG_ZIP'),
            'city'=>env('ORG_CITY'),
            'country'=>env('ORG_COUNTRY'),
            'email'=>env('ORG_EMAIL'),
        ];
        $data = ['reg'=>$reg, 'unit'=>$unit, 'DSO'=>$DSO, 'enva'=>$enva,];
        
        $pdf = PDF::loadView('pdf.regdesc', $data, [], [
            'format'=> 'A4',
            'author'=> $enva['name'],
            'creator'=> 'Reggee, Registerhanteraren',
            'keywords'=> $reg->name.', '.$reg->unit->name.', '.'GDPR'.', '.'registerbeskrivning',
        ]);
        $file = $reg->name . '-' . now() . '.pdf';
        
        $filename = public_path('pdf/'). $file;
        

        $pdf->save($filename);
        

        // The email containing regdesc is sent to unit contact, DPO and the sender.
        $receivers = [$reg->unit->email, $reg->unit->user];
        Mail::to(
            Auth::user())
            ->cc($receivers)
            ->send(new NewRDnotification($reg, $filename));
        
        File::delete($filename);
        //Storage::disk('local')->delete($filename);
        return redirect('home')->with('status', 'Mail skickat');
    }
}
