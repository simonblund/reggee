<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Register;
use Illuminate\Support\Facades\Storage;

class NewRDnotification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    public $register;
    public $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Register $register, String $pdf)
    {
        $this->register = $register;
        $this->pdf = $pdf;

    }
    

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('mail.RDnotification')
                    ->subject('Uppdaterad registerbeskrivning '.$this->register->name)
                    ->attach($this->pdf);
    }
}
