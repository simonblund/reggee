<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_admin', 'can_unit', 'can_register', 'connected_unit'
    ];
    public function units()
    {
        return $this->hasMany('App\Unit', 'user_id');
    }
    public function connected_unit()
    {
        return $this->hasMany('App\Unit', 'connected_unit');
    }
    
}
