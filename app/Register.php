<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sasin91\LaravelVersionable\Versionable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Register extends Model
{
    use Versionable;
    use SoftDeletes;


    protected $fillable = [
    'name', 'digital', 'reason', 
    'people_in_it', 'under_13', 'information_points', 'sensitive_data', 'people_with_access', 'outside_eu', 'save_or_archive_time',
    'legal_reason', 'consent_boolean', 'consent_information', 'collection_information', 'safety_systems', 'on_paper', 'on_computer', 'unit_id', 'internal_identification',
    'date_reviewed', 'reviewed_by', 'comments', 'edited_by', 'hash', 'breach_information_internal', 'breach_information_public', 'processor_name', 'processor_orgnr', 'processor_address', 'processor_city_zip', 'processor_phone',
];
protected $dates = ['deleted_at'];

public function unit()
    {
        return $this->belongsTo('App\Unit');
    }
    public function editor()
    {
        return $this->belongsTo('App\User', 'edited_by');
    }
    public function reviewer()
    {
        return $this->belongsTo('App\User', 'reviewed_by');
    }

}
