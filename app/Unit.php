<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Unit extends Model
{
    use Notifiable;

    protected $fillable = ['name', 'streetaddress', 'city', 'zip', 'telephone', 'email', 'contact', 'user_id'];
    
    public function register()
    {
        return $this->hasMany('App\Register', 'unit_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function routeNotificationForMail()
    {
        return $this->email;
    }
}
